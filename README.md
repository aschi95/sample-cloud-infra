# Sample cloud infrastructure project for Hello World REST API
## Table of contents

### 1. [Hello world API](#hello-world-api)
### 2. [CI/CD Pipeline](#cicd-pipeline)
### 3. [GKE Cluster](#gke-cluster)
### 4. [Monitoring](#monitoring)
### 5. [Logging](#logging)
### 6. [Scaling](#scaling)
  
   
  
# Hello world API
- [API](/helloworld-api/index.js) is located in /helloworld-api
- Written in JavaScript 
- Utilizes Node.js frameworks:
  - Express framework for processing requests and responses and 
  - Morgan framework to provide API logging (source IP, datetime, used http method, http version, http response code, response size etc.)
- Provides two endpoints on port 8080
  - /health for verifying readiness of app
  - /helloworld as main operation which returns message "Hello World!", name and IP address of pod in which it is running and kubernetes namespace
   
  
![Hello world API Response](/img/helloworld_api.png)
 
# CI/CD Pipeline
- [GitLab Pipeline](.gitlab-ci.yml)
- Every push to repository triggers pipeline
- 6 stages (3 test stages are only simulated)
  1. Build Docker image
  2. Execute unit tests (simulated, no tests are present, only for demonstration)
  3. Deploy to staging env (Kubernetes namespace "staging")
  4. Execute tests on staging env (simulated, no tests are present, only for demonstration)
  5. Deploy to prod env (Kubernetes namespace "production")
  6. Execute tests on prod env (simulated, no tests are present, only for demonstration)
- Using custom Docker image for gitlab-runner, [Dockerfile](/cluster-setup/custom-docker-image/Dockerfile) located in /setup/custom-docker-image
  - Based on docker:18.09.7 with added curl and kubectl for managing deployments to Kubernets cluster

# GKE Cluster
- Created in europe-west1-b	location
- 3 nodes, 6vCPUs and 22.5GB memory total
- Integrated with GitLab
- Services exposed through LoadBalancer
 
# Monitoring
- Provided with Prometheus and Grafana
- Dashboards for monitoring various aspects of the cluster
  - Pods
  - Deployments
  - Nodes
  - Compute resources allocation - Cluster, namespace or pod scope
  - Compute resources utilization - Cluster and node scope
  - Cluster health
  
![Grafana Cluster compute resources utilization](/img/grafana_cluster_utilization.png)
  
# Logging
- Secured by Morgan framework in Hello world API
- All logs collected by GKE logging system
 
# Scaling
- Autoscaling set up for Hello world APIs in staging and prod env
  - Threshold for autoscaling is 50% average CPU utilization across all pods
  - Minimum number of replicas 1, maximum 5
- Adding new service could be secured by the same pipeline template in different repository
- It would require change of Dockerfile, source files for service(index.js in this case) and value of env variable IMAGE_NAME that dictates Docker image tag