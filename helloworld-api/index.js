const express = require('express');
var morgan = require('morgan')
const app = express();

app.use(morgan('combined'))

const payload =
    {
        "message": 'Hello World!',
        "pod_name": process.env.POD_NAME || "",
        "pod_namespace": process.env.POD_NAMESPACE || "",
        "pod_ip": process.env.POD_IP || ""
    }

app.get('/helloWorld', function (req, res) {
    res.send(payload);
})

app.get('/health', function (req, res) {
    res.send({"status": 'Healthy!'});
})

app.listen(8080);
